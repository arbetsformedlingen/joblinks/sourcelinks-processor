## 3.1.0
* Update Python to 3.12.1
* Use Python Unittest to run tests
* Update README

## 3.0.0
* Use Python 3.10.7
* Use Poetry for dependency management

## 2.0.0
* Reworked to only include sources that are explicitly allowed
* Sources to import must be a comma-separated string
* Updated Python version in Docker

## 1.0.0
* Initial release
* set 'isValid' to False if ALL (and ONLY unwanted) unwanted source links is in ad
