from data_handler import DataHandler
from chaos import ChaosTest
from sourcelinks_processor import processor
import logging
import settings
import argparse

logging.basicConfig(filename=settings.LOG_FILE, format="%(asctime)s [%(levelname)s] %(message)s")

def run(chaos_test, sources_to_import):
    with DataHandler() as dh, ChaosTest(chaos_test):
        dh.ads = processor.process_all_ads(dh.ads, sources_to_import)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CLI Argument Handler')
    parser.add_argument('--chaos-test', type=bool, default=False, help='activate chaos gamling mode')
    parser.add_argument('--sources-to-import', type=str, default="", help='comma separated list of sources to import')
    args = parser.parse_args()

    run(args.chaos_test, args.sources_to_import)