import sys
import random
import logging
import settings

logging.basicConfig(level='INFO',
                    format="%(asctime)s [%(levelname)s] %(message)s",
                    handlers=[logging.FileHandler(settings.LOG_FILE, mode='w', encoding='utf-8')])


class ChaosTest():
    """
        if command line arg --chaos-test is used, there should be 5% risk of exit
        this test is executed automatically when this class is used with a 'with' statement
        e.g 'with ChaosTest:'
    """

    def __init__(self, run_chaos_test=False):
        self.run_chaos_test = run_chaos_test
        self.error = self._potential_chaos()

    def _potential_chaos(self):
        if self.run_chaos_test:
            logging.info('chaos test started')
            return random.randint(1, 20) == 1
        else:
            return False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if self.error and self.run_chaos_test:
            logging.error("chaos test failed")
            sys.exit(1)
        elif self.run_chaos_test:
            logging.info("chaos test passed")
            return True
