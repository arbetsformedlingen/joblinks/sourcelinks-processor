# sourcelinks-processor

## Handles links to websites where an ad has been published

Sourcelinks are one or more links to the site(s) where the ad has been published and scraped.
The purpose of this processor is to only include links to sites that have agreed to be included in JobAd Links.

Which sourcelink(s) to include is controlled by the command-line argument `--sources-to-import`.
This argument takes either a single site as a string, or multiple sites separated by commas.
e.g.:
include one site: `--sources-to-import site`
include multiple sites: `--sources-to-import site_1,site_2,site_3`

The ads have a field called `isValid`. Only ads where this value is `True` will be imported by joblinks-importer
or elastic-importer.
if no `--sources-to-import` argument is provided, the field `isValid` will be set to `False` in all ads.
If an ad has no source links at all that are in the `--sources-to-import` argument, the field `isValid`
is set to `False`.

If an ad has ANY sources that should be imported, these will be in the `sourceLinks` field.
All source links will be copied to a new field; `original_source_links` which will NOT be imported. It's kept
for troubleshooting.

When running as part of the JobLinks pipeline, the ads are received through stdin and sent to the next pipeline
step through stdout.

## Prerequisites

Read the [Python guidelines](https://gitlab.com/arbetsformedlingen/documentation/-/blob/main/engineering/plattform/python-projects.md)

Install dependencies with `poetry install`

## Environment variables

| Variable            | Comment
|---------------------|---------
| `USE_STDIN`         | default value `True`
| `FILE_NAME`         | default value 'output.json', used if `USE_STDIN` is `False`
| `LOG_LEVEL`         | default value `INFO`
| `LOG_FILE`          | default value "sourcelinks-processor-log"
| `PRINT`             | default value `True`. Determines if ads should be written to stdout at completion.

## Logging

Logging is done to file or stderr.

## Tests

```sh
python -m unittest tests -v
```

The reason to use unitest instead of Pytest is that it's a single test file with simple tests.
unittest is included in Python, and we don't need to keep track of a separate dependecy for tests.

## Usage

```sh
python main.py
```

Optional argument `--chaos-test` will cause 5% risk of exiting immediately with error code `1`.

## CI
The Dockerfile has been removed in favor of [the Jobtech Python Poetry CI](https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/tree/master/python-poetry).
