import os


def get_bool_env_var(env_var_name, default):
    default = str(default)
    return os.environ.get(env_var_name, default).lower() == 'true'


USE_STDIN = get_bool_env_var("USE_STDIN", default=True)
READ_FROM_FILE_NAME = os.environ.get("FILE_NAME", default='output.json')
LOG_LEVEL = os.environ.get("LOG_LEVEL", default="INFO")
LOG_FILE = os.environ.get("LOG_FILE", default="sourcelinks-processor.log")
PRINT = get_bool_env_var("PRINT", default=True)