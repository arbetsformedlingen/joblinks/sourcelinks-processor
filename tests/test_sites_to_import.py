import unittest

from sourcelinks_processor.processor import keep_sources_to_import

original_source_links = [
    {
        "displayName": "arbetsformedlingen.se",
        "link": "https://arbetsformedlingen.se/12345",
    },
    {"displayName": "something_1", "link": "https://something_1/12345"},
    {"displayName": "something_2", "link": "https://something_2/12345"},
    {"displayName": "something_3", "link": "https://something_else/12345"},
]


class TestSourceLinks(unittest.TestCase):
    def test_keep_sites_to_import(self):
        expected_source_links = [
            {"displayName": "something_1", "link": "https://something_1/12345"},
        ]
        ad_original = {"id": "abc123", "sourceLinks": original_source_links}
        sites_to_import = ["something_1", "something_4"]

        ad_processed = keep_sources_to_import(ad_original, sites_to_import)

        assert ad_processed["sourceLinks"] == expected_source_links
        expected_ad = {
            "id": "abc123",
            "isValid": True,
            "original_source_links": original_source_links,
            "sourceLinks": [
                {"displayName": "something_1", "link": "https://something_1/12345"}
            ],
        }
        assert ad_processed == expected_ad

    def test_sites_to_import_not_in_ad(self):
        """
        None of the sites to import are in the ad
        sourceLinks should be empty
        original_source_links should not have changed
        """

        ad_original = {"id": "abc123", "sourceLinks": original_source_links}
        sites_to_import = ["not_in_ad_1", "not_in_ad_2"]

        ad_processed = keep_sources_to_import(ad_original, sites_to_import)

        assert ad_processed["sourceLinks"] == []
        assert ad_processed["original_source_links"] == original_source_links
        assert not ad_processed["isValid"]
