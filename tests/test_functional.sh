#!/bin/sh

## run python version:
#RUN_APP="env PRINT=True LOG_FILE=/dev/stderr python main.py"

## run static binary version
RUN_APP="env PRINT=True LOG_FILE=/dev/stderr ./main"

## run docker version
#RUN_APP="docker run --rm -i $BRANCH_IMAGE"



if [ $# -gt 0 ]; then
    RUN_APP=$@
fi

jq -S -c -s '. | sort_by(.id) | .[]' < tests/testdata/expected_output.10 > /tmp/expected_output.10 || exit 1

$RUN_APP --sources-to-import 2006.100.jsonl < tests/testdata/input.10 | jq -S -c -s '. | sort_by(.id) | .[]' > /tmp/output.10 || exit 1

diff /tmp/expected_output.10 /tmp/output.10 || exit 1

echo "The program passed a diff test" >&2
