#!/bin/sh

script_dir="$(dirname "$(readlink -f "$0")")"

sh "$script_dir"/test_unittest.sh || exit 1
sh "$script_dir"/test_functional.sh || exit 1
