import sys
from datetime import datetime
import logging
import settings


def keep_sources_to_import(ad, sources_to_import):
    """
    if there are no match for sites to import, the ad 'isValid' attribute will
    be set to False and the ad will not be imported
    If no sites are provided, the ad will have the 'isValid' attribute set to False,
     preventing any ad from being imported
    """
    if not sources_to_import:
        ad['isValid'] = False
        return ad

    ad['original_source_links'] = ad['sourceLinks']  # keep original data
    ad['isValid'] = False
    source_links_to_keep = []
    for source_link in ad['sourceLinks']:
        if source_link['displayName'] in sources_to_import:
            source_links_to_keep.append(source_link)
            ad['isValid'] = True
    ad['sourceLinks'] = source_links_to_keep
    return ad


def get_display_names_from_ad(ad):
    source_links = []
    for link in ad['sourceLinks']:
        source_links.append(link['displayName'])
    return source_links


def process_all_ads(all_ads, sources_to_import):
    start = datetime.now()
    logging.info("start processing ads")
    converted_ads = []

    sources_to_import = list(sources_to_import.split(','))
    for ad in all_ads:
        checked_ad = keep_sources_to_import(ad, sources_to_import)
        converted_ads.append(checked_ad)
    elapsed = datetime.now() - start
    logging.info(f"{len(all_ads)} ads processed in {elapsed.total_seconds()} seconds")
    return converted_ads


def print_and_log_warning(warning_msg):
    logging.warning(warning_msg)
    print(f"WARNING - {warning_msg}", file=sys.stderr)


def print_and_log_error(error_msg):
    logging.error(error_msg)
    print(error_msg, file=sys.stderr)
